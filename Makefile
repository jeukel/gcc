# Consider the following declaration:
#
#   all: library.cpp main.cpp
#
# In this case:
#
#   $@ evaluates to all
#   $< evaluates to library.cpp
#   $^ evaluates to library.cpp main.cpp

.PHONY: clean

calc-dynamic: libmath.so
	gcc -o calc-dynamic Calculadora.c -L. -lmath -lm

libmath.so:
	gcc -c -fPIC mathlib.c -lm
	gcc mathlib.o -shared -o libmath.so

calc-static: libmath.a
	gcc -o calc-static Calculadora.c libmath.a -lm

libmath.a:
	gcc -c mathlib.c -lm
	ar rv libmath.a mathlib.o

clean:
	rm -f *.o *.a *.so calc-dynamic calc-static
