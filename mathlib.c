#include "mathlib.h"

double suma(double a, double b)
{
  return a+b;
}

double resta(double a, double b)
{
  return a-b;
}

double multi(double a, double b)
{
  return a*b;
}

double div(double a, double b)
{
  return a/b;
}

double raiz(double a)
{
  return sqrt(a);
}
