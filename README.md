# Evaluación Tutorial 2 GCC

## 1. ¿Qué es GCC?
GCC o la colección de compiladores de GNU (The GNU Compiler Collection) incluye interfaces para C, C ++, Objective-C, Fortran, Ada y Go, así como bibliotecas para estos idiomas (libstdc++,...). GCC fue originalmente escrito como el compilador del sistema operativo GNU. El sistema GNU fue desarrollado para ser un software 100% gratuito, gratuito en el sentido de que respeta la libertad del usuario.

## 2. ¿Cuáles son las 4 etapas de compilación?
1. Preprocesado:
Son operaciones de inclusión, búsqueda y sustitución de textos, borrado de otros. Son como las realizadas por un procesador de textos.

2. Compilación a lenguaje ensamblador:
Da  un  archivo  en  ensamblador  con  extensión  s.  (Usa  la directiva -S).

3. Compilación a código máquina:
Genera código máquina no ejecutable que pone en un archivo con extensión o.

4. Enlace:
Genera el enlace con las bibliotecas disponibles en código máquina para generar el ejecutable.

## 3. ¿Qué comando deber ́ıa utilizar para generar el código en emsamblador de un archivo fuente, por ejemplo, calculadora.c?
	gcc -S -o myFileOutput.s calculadora.c

## 4. ¿Cuál es la diferencia entre biblioteca estática y una dinámica?
La diferencia de ambos tipo es la de cómo están ligadas al programa a ejecutarse. Para una biblioteca estática las funciones necesarias de esa librería se copian en tu ejecutable. Una librería dinámica se carga en el momento de la ejecución del programa.

## 5. Lista de comandos para creación de bibliotecas

### Procedimiento manual
#### Biblioteca Estática

1. `gcc -c mathlib.c -lm`
2. `ar rv libmath.a mathlib.o`
3. `ranlib libmath.a`
4. `gcc -o math Calculadora.c libmath.a -lm`

#### Biblioteca Dinámica

1. `gcc -c mathlib.c -lm`
2. `ld -o libmath.so mathlib.o -shared`
3. `gcc -o calcsh Calculadora.c -Bdynamic libmath.so -lm`
4. `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`*/path_to_folder*

### Procedimiento *automatico*
#### Biblioteca Estática

```
make libmath.a
```

### Biblioteca Dinámica

```
make libmath.so
```

## 6. Lista de comandos para linkear contra bibliotecas

### Linkear contra biblioteca estática

```
make calc-static
./calc-static
```

### Linkear contra biblioteca dinámica

```
make calc-dynamic
LD_LIBRARY_PATH=. ./calc-dynamic
```
