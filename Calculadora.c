/*
 * Calculadora.c
 *
 * Copyright 2018 Daniel Jenkins <jeukel@Galago>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdio.h>
#include "mathlib.h"

double a = 10;
double b = 5;

int main()
{
  double s;
  
  printf("La suma de %f y %f es:\n", a, b );
  s = suma(a,b);
  printf("%lf",s);
  printf("\n");
  
  printf("La resta de %f y %f es:\n", a, b);
  s = resta(a,b);
  printf("%lf",s);
  printf("\n");
  
  printf("La multiplicación de %f y %f es:\n", a, b);
  s = multi(a,b);
  printf("%lf",s);
  printf("\n");
  
  printf("La división de %f y %f es:\n", a, b);
  s = div(a,b);
  printf("%lf",s);
  printf("\n");

  printf("La raiz de %f es:\n",b);
  s = raiz(b);
  printf("%lf",s);
  printf("\n"); 
  
  return 0;
}
